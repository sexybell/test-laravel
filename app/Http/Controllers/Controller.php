<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Log;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function requestLog($request, $datetime, $dataRequest = [])
    {
        $dataLogs = [
            'datetime' => $datetime,
            'http_method' => $request->method(),
            'uri' => $request->getRequestUri(),
            'dataRequest' => $dataRequest,
        ];

        return Log::channel('request_log')->info('', $dataLogs);
    }

    public function responseLog($request, $datetime, $dataResponse, $error = false)
    {
        $dataLogResponses = [
            'datetime' => $datetime,
            'http_method' => $request->method(),
            'uri' => $request->getRequestUri(),
            'dataResponse' => $dataResponse,
        ];

        if ($error) {
            return Log::channel('response_error_log')->info('', $dataLogResponses);
        } else {
            return Log::channel('response_success_log')->info('', $dataLogResponses);
        }
    }
}

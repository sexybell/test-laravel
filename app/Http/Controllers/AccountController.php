<?php

namespace App\Http\Controllers;

use App\Services\AccountService;
use Illuminate\Http\Request;
use PHPUnit\Util\Exception;

class AccountController extends Controller
{

    protected $accountService;

    protected $dateString;

    public function __construct(AccountService $accountService)
    {
        $this->accountService = $accountService;
        $this->dateString = now()->format('YmdHis');
    }

    public function list(Request $request)
    {
        try {
            $params = $request->all();
            $this->requestLog($request, $this->dateString, $params);
            $list = $this->accountService->list($params);
            $this->responseLog($request, $this->dateString, $list);
            return response()->json(['data' => $list, 'msg' => 'Get Data Successfully']);
        } catch (Exception $e) {
            $this->responseLog($request, $this->dateString, $e, true);
            report($e);
            return response()->json(['data' => [], 'msg' => $e->getMessage()], 203);
        }

    }

    public function getOne(Request $request)
    {
        try {
            $id = $request->only('id');
            if ($id) {

            }
        } catch (Exception $e) {
            $this->responseLog($request, $this->dateString, $e, true);
            report($e);
            return response()->json(['data' => [], 'msg' => $e->getMessage()], 203);
        }
    }

    public function save(Request $request)
    {

    }

    public function delete(Request $request)
    {
        try {
            $id = $request->only('id');
            if ($id) {

            }
        } catch () {

        }
    }
}

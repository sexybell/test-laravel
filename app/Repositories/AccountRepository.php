<?php

namespace App\Repositories;

use App\Models\Accounts;

class AccountRepository
{
    protected Accounts $accounts;

    public function __construct(Accounts $accounts)
    {
        $this->accounts = $accounts;
    }

    public function list($params)
    {
        $query = $this->accounts->query()->get();
        return $query;
    }

    public function getOne($params)
    {
        $query = $this->accounts->query()->where('id', $params['id'])->first();
        return $query;
    }

    public function save($params)
    {
        $query = $this->accounts->query()->updateOrCreate($params);
        return $query;
    }

    public function delete($params)
    {
        $query = $this->accounts->query()->where('id', $params['id'])->update(['deleted_at' => now()]);
        return $query;
    }
}

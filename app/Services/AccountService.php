<?php

namespace App\Services;

use App\Repositories\AccountRepository;

class AccountService
{
    protected AccountRepository $accountRepository;

    public function __construct(AccountRepository $accountRepository)
    {
        $this->accountRepository = $accountRepository;
    }

    public function list($params)
    {
        return $this->accountRepository->list($params);
    }

    public function getOne($params)
    {
        return $this->accountRepository->getOne($params);
    }

    public function save($params)
    {
        return $this->accountRepository->save($params);
    }

    public function delete($params)
    {
        return $this->accountRepository->delete($params);
    }
}

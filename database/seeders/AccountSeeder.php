<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class AccountSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('accounts')->truncate();
        for ($i = 0; $i < 30; $i++) {
            DB::table('accounts')->insert([
                'login' => Str::random(5).'@example.com',
                'phone' => '0336678028',
                'password' => '12345678',
            ]);
        }
    }
}
